package com.example.zoo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.HashMap;
import android.widget.ArrayAdapter;

//import static com.example.zoo.AnimalList.init;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView firstListView = (ListView) findViewById(R.id.listView);

        //HashMap<String, Animal> zoo = AnimalList.init();

        //AnimalList zoo = new AnimalList();


        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, AnimalList.getNameArray());
        firstListView.setAdapter(adapter);

        firstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View V, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), Detail.class);
                String message = (String) parent.getItemAtPosition(position);
                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });

    }
}
